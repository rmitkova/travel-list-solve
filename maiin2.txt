#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_NODES 100
#define INF 1000000000

typedef struct {
    char name[20];
    int delay_factor;
} Transition;

typedef struct {
    char name[20];
    Transition *transitions[MAX_NODES];
    int num_transitions;
} Node;

Node *nodes[MAX_NODES];
int num_nodes = 0;

void read_input_file(char *filename) {
    printf("Filename: %s\n", filename);  // Debug statement
    FILE *fp = fopen(filename, "r");

    if (fp == NULL) {
        printf("Error opening file: %s\n", filename);
        return;
    }

    char line[100];
    while (fgets(line, 100, fp)) {
        char *node_name = strtok(line, " =>,");
        Node *node = malloc(sizeof(Node));
        strcpy(node->name, node_name);
        node->num_transitions = 0;
        nodes[num_nodes++] = node;

        char *transition_str = strtok(NULL, " =>,");
        while (transition_str != NULL) {
            char *comma = strchr(transition_str, ',');
            char *factor_str = strchr(transition_str, '>');
            if (comma == NULL || factor_str == NULL) {
                break;
            }
            *comma = '\0';
            *factor_str = '\0';
            factor_str++;
            int factor = atoi(factor_str);
            Transition *transition = malloc(sizeof(Transition));
            strcpy(transition->name, transition_str);
            transition->delay_factor = factor;
            node->transitions[node->num_transitions++] = transition;
            transition_str = strtok(NULL, " =>,");
        }
    }

    fclose(fp);
}

int dijkstra(int start, int end) {
    int dist[MAX_NODES];
    int visited[MAX_NODES];
    int parent[MAX_NODES];
    int i, j;

    // Initialize arrays
    for (i = 0; i < num_nodes; i++) {
        dist[i] = INF;
        visited[i] = 0;
        parent[i] = -1;
    }
    dist[start] = 0;

    // Find shortest path
    for (i = 0; i < num_nodes - 1; i++) {
        // Find node with minimum distance
        int min_dist = INF;
        int u = -1;
        for (j = 0; j < num_nodes; j++) {
            if (!visited[j] && dist[j] < min_dist) {
                min_dist = dist[j];
                u = j;
            }
        }
        if (u == -1) {
            break;
        }
        visited[u] = 1;

        // Update distances of neighboring nodes
        for (j = 0; j < nodes[u]->num_transitions; j++) {
            Transition *transition = nodes[u]->transitions[j];
            int v = -1;
            // Find node index of transition destination
            for (int k = 0; k < num_nodes; k++) {
                if (strcmp(nodes[k]->name, transition->name) == 0) {
                    v = k;
                    break;
                }
            }
            if (v == -1) {
                printf("Error: Could not find node '%s'\n", transition->name);
                return -1;
            }
            int alt = dist[u] + transition->delay_factor;
            if (alt < dist[v]) {
                dist[v] = alt;
                parent[v] = u;
            }
        }
    }

// Print shortest path
    if (parent[end] == -1) {
        printf("No path from node '%s' to node '%s'\n", nodes[start]->name, nodes[end]->name);
        return -1;
    }

    printf("Shortest path from node '%s' to node '%s':\n", nodes[start]->name, nodes[end]->name);
    printf("%s", nodes[start]->name);
    int current = end;
    while (current != start) {
        printf(" -> %s", nodes[current]->name);
        current = parent[current];
    }
    printf("\n");

    return dist[end];
}

int main(int argc, char *argv[]) {
    if (argc < 4) {
        printf("Usage: %s <filename> <start_node> <end_node>\n", argv[0]);
        return 0;
    }
    char *filename = argv[1];
    int start = -1;
    int end = -1;

    read_input_file(filename);

// Find node indices of start and end nodes
    for (int i = 0; i < num_nodes; i++) {
        if (strcmp(nodes[i]->name, argv[2]) == 0) {
            start = i;
        }
        if (strcmp(nodes[i]->name, argv[3]) == 0) {
            end = i;
        }
    }
    if (start == -1) {
        printf("Error: Could not find start node '%s'\n", argv[2]);
        return 0;
    }
    if (end == -1) {
        printf("Error: Could not find end node '%s'\n", argv[3]);
        return 0;
    }

    int distance = dijkstra(start, end);
    printf("Shortest distance: %d\n", distance);

    return 0;
}
